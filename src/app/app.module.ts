import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes, Route } from '@angular/router';

import { AppComponent } from './app.component';
import { WeatherComponent } from './components/weather/weather.component';
import { WeatherService } from './services/weather.service';
import { MainComponent } from './components/main/main.component';
import { IconComponent } from './components/icon/icon.component';
import { WeatherIconComponent } from './components/weather-icon/weather-icon.component';

const appRoutes:Routes = [
  {path:"",component:MainComponent},
  {path:"search/:keyword",component:MainComponent},
  {path:"weather/:woeid",component:MainComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    MainComponent,
    IconComponent,
    WeatherIconComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [WeatherService],
  bootstrap: [AppComponent]
})
export class AppModule { }
