import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-icon',
  templateUrl: './weather-icon.component.html',
  styleUrls: ['./weather-icon.component.css']
})
export class WeatherIconComponent implements OnInit {
  
  @Input() weatherIcon:string;
  iconPath:string;

  constructor() { }

  ngOnInit() {
    if(this.weatherIcon=='Snow'){
      this.iconPath="https://www.metaweather.com/static/img/weather/sn.svg";
    }
    else if(this.weatherIcon=='Sleet')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/sl.svg";
    }
    else if(this.weatherIcon=='Hail')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/h.svg";
    }
    else if(this.weatherIcon=='Thunderstorm')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/t.svg";
    }
    else if(this.weatherIcon=='Heavy Rain')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/hr.svg";
    }
    else if(this.weatherIcon=='Light Rain')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/lr.svg";
    }
    else if(this.weatherIcon=='Showers')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/s.svg";
    }
    else if(this.weatherIcon=='Heavy Cloud')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/hc.svg";
    }
    else if(this.weatherIcon=='Light Cloud')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/lc.svg";
    }
    else if(this.weatherIcon=='Clear')
    {
      this.iconPath="https://www.metaweather.com/static/img/weather/c.svg";
    }
    
  }

}
