import { Component, Input } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent  {

  @Input() data:any[];

  @Input() SearchResult:number = 0;
  woeid:string;
  DetailsPage:number = 0;
  
  constructor( private route: ActivatedRoute ) {}

  ngOnInit() {

   

    if(this.route.snapshot.paramMap.get('woeid'))
    {
      this.woeid = this.route.snapshot.paramMap.get('woeid');
      this.DetailsPage = 1;
    }

    if(this.SearchResult==1)
    {
      this.DetailsPage = 1;
    }

    console.log('Data is:',this.data);
  }

 

}
