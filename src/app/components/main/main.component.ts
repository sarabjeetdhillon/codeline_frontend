import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';

import { WeatherService } from '../../services/weather.service';



@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  BerlinDetails:WeatherDetailInterface[];
  IstanbulDetails:WeatherDetailInterface[];
  LondonDetails:WeatherDetailInterface[];
  HelsinkiDetails:WeatherDetailInterface[];
  DublinDetails:WeatherDetailInterface[];
  VancouverDetails:WeatherDetailInterface[];

  searchResult:number= 0;
  searchedCityDetails:any;

 

  constructor(private weatherService:WeatherService) {
    
   }

  ngOnInit() {
    
   
    this.weatherService.getWeatherBerlin().subscribe((BerlinDetails) => {
      // console.log(BerlinDetails);
      this.BerlinDetails=BerlinDetails;
    });
    this.weatherService.getWeatherIstanbul().subscribe((IstanbulDetails) => {
      // console.log(IstanbulDetails);
      this.IstanbulDetails=IstanbulDetails;
    });
    this.weatherService.getWeatherLondon().subscribe((LondonDetails) => {
      // console.log(LondonDetails);
      this.LondonDetails=LondonDetails;
    });
    this.weatherService.getWeatherHelsinki().subscribe((HelsinkiDetails) => {
      // console.log(HelsinkiDetails);
      this.HelsinkiDetails=HelsinkiDetails;
    });
    this.weatherService.getWeatherDublin().subscribe((DublinDetails) => {
      // console.log(DublinDetails);
      this.DublinDetails=DublinDetails;
    });
    this.weatherService.getWeatherVancouver().subscribe((VancouverDetails) => {
      // console.log(VancouverDetails);
      this.VancouverDetails=VancouverDetails;
    });
  }

  submitSearch(keyword){
    this.weatherService.getSearchedCity(keyword).subscribe((citydata) => {
      // console.log(citydata[0].woeid);
      let woeid = citydata[0].woeid;
      this.weatherService.getCityByWoeid(woeid).subscribe((citydetails) => {
        console.log(citydetails);
        this.searchResult = 1;
        this.searchedCityDetails = citydetails;
      });  
      // this.VancouverDetails=citydata;
    });
    // console.log(keyword);
    return false;
  }

}

interface WeatherDetailInterface{
  weather_state_name:string,
  min_temp:string,
  max_temp:string,
  the_temp:string,
}