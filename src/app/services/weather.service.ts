import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  weatherDetails:any[];

  constructor(public http:Http) { 
    console.log("weather service connected..");
  }

  getWeatherBerlin(){
    return this.http.get('http://localhost.local/misc/weather.php?command=location&woeid=638242').pipe(
      map(
        res=>res.json()
      )
    );
  }

  getWeatherIstanbul(){
    return this.http.get('http://localhost.local/misc/weather.php?command=location&woeid=2344116').pipe(
      map(
        res=>res.json()
      )
    );
  }
  
  getWeatherLondon(){
    return this.http.get('http://localhost.local/misc/weather.php?command=location&woeid=44418').pipe(
      map(
        res=>res.json()
      )
    );
  }

  
  getWeatherHelsinki(){
    return this.http.get('http://localhost.local/misc/weather.php?command=location&woeid=565346').pipe(
      map(
        res=>res.json()
      )
    );
  }
  
  getWeatherDublin(){
    return this.http.get('http://localhost.local/misc/weather.php?command=location&woeid=560743').pipe(
      map(
        res=>res.json()
      )
    );
  }

  getWeatherVancouver(){
    return this.http.get('http://localhost.local/misc/weather.php?command=location&woeid=9807').pipe(
      map(
        res=>res.json()
      )
    );
  }

  getSearchedCity(cityname){
    return  this.http.get('http://localhost.local/misc/weather.php?command=search&keyword='+cityname).pipe(
      map(
        res=>res.json()
      )
    );
  }

  getCityByWoeid(woeid){
    return  this.http.get('http://localhost.local/misc/weather.php?command=location&woeid='+woeid).pipe(
      map(
        res=>res.json()
      )
    );
  }


  
}
